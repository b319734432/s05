const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		if(req.body.hasOwnProperty('alias', 'amount')){
			return exchangeRates
		}

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be string'
			})
		}

		if(typeof !req.body.name !== 'string'){
			return res.status(400).send({
				'error': 'Bad Request - Name has to be a string'
			})
		}

		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': 'Bad Request: missing required parameter: name'
			})
		}

		if(typeof req.body.age !== 'number'){
			return res.status(400).send({
				'error': 'Bad Request - Age has to be a number'
			})
		}
	})
}
